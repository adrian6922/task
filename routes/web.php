<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect('/login'); });
Route::post('/login-user', 'Auth\LoginController@login');

Route::resource('tasks','TaskController');
Route::group(['prefix' => 'task'], function(){
    Route::get('getTask','TaskController@getTask');
    Route::post('finished','TaskController@finished');
});
Route::resource('users','UserController');
Route::group(['prefix' => 'user'], function(){
    Route::get('verify-email/{email?}','UserController@verify_email');
    Route::post('change-password','UserController@change_password');
});
Auth::routes();
Route::get('/home','HomeController@index')->name('home');
Route::get('/{any}',function(){ return view('error.index');});
