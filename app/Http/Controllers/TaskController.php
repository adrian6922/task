<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB as FacadesDB;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('task.index');
    }

    public function getTask(){
        $modelTask = new Task();
        $tasks = $modelTask->getTask();
        return response()->json( $tasks );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'task' => 'required',
            'task_description' => 'required',
        ]);
    
        if ($validator->fails()) {
            toastr()->warning($validator->errors()->first());
            return back()->withInput($request->all());
        }
    
        try {
            FacadesDB::beginTransaction();
    
            $user = auth()->user()->idUser;
    
            $task = Task::create([
                'idUser' => $user,
                'task' => $request->task,
                'description' => $request->task_description,
            ]);
    
            FacadesDB::commit();
    
            return response()->json(1);
        } catch (\Exception $e) {
            toastr()->warning($e->getMessage());
            FacadesDB::rollback();
            return back()->withInput($request->all());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function finished(Request $request){
        $validator = Validator::make($request->all(), [
            'idTask' => 'required',
        ]);
    
        if ($validator->fails()) {
            toastr()->warning($validator->errors()->first('idTask'));
            return back()->withInput($request->all());
        }
    
        try {
            FacadesDB::beginTransaction();
    
            Task::where('idTask', $request->idTask)
                ->update([
                    'finished' => 1,
                    'updated_at' => Carbon::now(),
                ]);
    
            FacadesDB::commit();
    
            return response()->json(1);
        } catch (\Exception $e) {
            toastr()->warning($e->getMessage());
            FacadesDB::rollback();
            return back()->withInput($request->all());
        }

    }
}
