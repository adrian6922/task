<div class="menu_nav">
    <div class="container">
        <div class="row">
            <div class="col-4 rounded">
                <img 
                    class="mt-1 rounded img_navbar"
                    src="{{ asset('assets/img/logo_prueba.png') }}"
                    alt="img_navbar"
                >
            </div>
            <div class="col d-flex align-items-center ml-3">
                <h6 class="text-start text-light">Meteor Task</h6>
            </div>
        </div>
    </div>
    <ul class="nav flex-column mt-2">
        <li class="nav-item item_menu">
            <a 
                class="nav-link active ms-5"
                aria-current="page"
                href="{{ url('/tasks') }}">
                <i class="fa-solid fa-clipboard-list"></i> Board
            </a>
        </li>
        <li class="nav-item item_menu">
            <a 
                class="nav-link ms-5"
                href="{{ url('users/'.Auth::user()->idUser) }}">
                <i class="fas fa-user-circle"></i> Mi perfil
            </a>
        </li>
        <li class="nav-item item_menu">
            <a 
                class="nav-link ms-5"
                href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <i class="fa-solid fa-arrow-right-to-bracket"></i> Cerrar sessión
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </li>
    </ul>
</div>
     