<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/logo_prueba.ico') }}"/>
        <title>Meteor Task | @yield('title')</title>
        @include('include.css')
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <aside class="d-none d-md-block col-md-3">
                    @include('include.menu')
                </aside>
                <div class="col-12 d-md-none">
                    @include('include.hamburguer_menu')
                </div>
                <div class="col-12 col-md-9">
                    <main>
                        @yield('content')
                    </main>
                </div>
            </div>
            @yield('script')
            @include('include.script')
        </div>
    </body>
</html>
<script>
    let mobileMenu = document.getElementById('mobileMenu');
    let navbarCollapse = document.getElementById('navbarCollapse');
    
    navbarCollapse.addEventListener('click', function() {
      if (mobileMenu.style.display === 'none') {
        mobileMenu.style.display = 'block';
      } else {
        mobileMenu.style.display = 'none';
      }
    });
</script>
