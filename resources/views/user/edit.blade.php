@extends('layouts.task.master')
@section('title','Editar Usuario')
@section('content')
    <style>
        [v-cloak] {
        display: none;   
    }
    </style>
    <div class="container-fluid" id="appEditUser">
        <div class="col-12 col-sm-8 col-md-6 col-lg-4 col-xl-4 col-xxl-4 mx-auto mb-5">
            <div v-cloak v-for="(user, index) in dataUser">
                <div class="container d-flex align-items-center rounded-circle bg-success circle_data_user">
                    <h2 class="ms-3 text-light">
                        @{{ user.initialName }}@{{ user.initialLastName }}
                    </h2>
                </div>
                <h2 class="text-center">
                    @{{ user.name }} @{{ user.last_name }}
                </h2>
            </div>
            <form 
                class="mt-5" 
                id="form_edit_password">
                @csrf
                <div class="mb-3">
                    <label 
                        class="text-secondary"
                        for="password">
                        Cambiar contraseña
                    </label>
                    <input 
                        class="form-control"
                        id="password"
                        type="password"
                        name="password"
                        v-model="inputPassword"
                        placeholder="ingrese contraseña"
                    >
                </div>
                <div class="mb-3">
                    <label 
                        class="text-secondary"
                        for="verifyPassword">
                        Confirmar contraseña
                    </label>
                    <input 
                        class="form-control"
                        id="verifyPassword"
                        type="password"
                        name="verifyPassword"
                        v-model="inputverifyPassword"
                        placeholder="ingrese contraseña nuevamente">
                </div>
                <div class="d-grid gap-2 col-10 col-sm-8 col-md-6 mx-auto mt-4">
                    <button 
                        class="btn btn-outline-warning"
                        id="changePassword"
                        type="submit"
                        @click.prevent="changePassword">
                        Guardar
                    </button>
                    <a 
                        class="btn btn-outline-success"
                        type="button"
                        href="{{ url('/home') }}">
                        Volver
                    </a>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')    
@include('include.vue')
    <script>
        new Vue({
            el:'#appEditUser',
            data() {
                return {
                    dataUser:[],
                    initialUser:'',
                    inputPassword:'',
                    inputverifyPassword:'',
                    idUser:'',
                }
            },
            methods: {
                getUser(){
                    axios.get('/users/1/edit')
                    .then(response=>{
                        this.dataUser = response.data
                    })
                    .catch(error=> {
                        console.log(error)
                    })
                },
                changePassword(){

                    let password =  document.getElementById('password');
                    let verifyPassword =  document.getElementById('verifyPassword');
                    let token = $('input[name=_token]').val();

                    if(this.validate_field(this.inputPassword)){
                        password.focus()
                        Swal.fire('¡Falta contraseña!','Complete campo contraseña','warning')
                        return
                    }

                    if(this.validate_field(this.inputverifyPassword)){
                        verifyPassword.focus()
                        Swal.fire('¡Falta confirma contraseña!','Complete campo confirmar contraseña','warning')
                        return
                    }

                    if(this.inputPassword != this.inputverifyPassword){
                        Swal.fire('¡Las contraseña no son iguales!', 'Intente de nuevo', 'warning');
                        this.cleanForm();
                        return
                    }

                    axios.post('/user/change-password',{
                        _token  : token,
                        password: this.inputPassword,
                        idUser  : this.idUser
                    })
                    .then(response=>{
                        if(response.data != 0 ){
                            Swal.fire('¡Se ha actualizado contraseña!', 'Correctamente!', 'success');
                            this.cleanForm();
                        }else{
                            Swal.fire('¡No se pudo actualizar contraseña!', 'Intente de nuevo', 'warning');
                            this.cleanForm();
                            return;
                        }
                    }).catch(error=>{
                        console.log(error)
                    }) 

                },
                validate_field(field){
                    return field == '' ? true : false;
                },
                cleanForm(){
                    return document.getElementById('form_edit_password').reset();
                }
            },
            mounted() {
                this.getUser();
                this.idUser = {{ $idUser }}
            },
        })
    </script>
@endsection