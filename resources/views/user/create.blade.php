@extends('layouts.task.login')
@section('title','Crear cuenta')
@section('content')
    <div 
        class="container-fluid"
        id="appCreateUser">
        <div class="col-12 col-sm-4 col-md-3 col-lg-3 col-xl-3 col-xxl-3 mx-auto mb-5 ">
            <div class="row container">
                <div class="col">
                    <img 
                        class="rounded logo_img"
                        src="{{ asset('assets/img/logo_prueba.png') }}"
                        alt="logo_login"
                    >
                </div>
                <div class="col d-flex align-items-center ml-3">
                    <h2 class="text-dark mt-4">Meteor Task</h2>
                </div>
            </div>
        </div>
            <div class="col-12 col-sm-4 col-md-4 col-lg-3 col-xl-3 col-xxl-3 mx-auto border border-success p-4 bg-success rounded">
                <h3 class="text-center text-light">Crear Cuenta</h3>
                <form id="form_register">
                    @csrf
                    <label 
                        class="text-light" 
                        for="name">
                        Nombre 
                        <span 
                            class="text-danger">
                            *
                        </span>
                    </label>
                    <input 
                        class="form-control"
                        id="name"
                        type="text"
                        name="name"
                        v-model="name"
                        placeholder="Ingrese nombre"
                    >

                    <label
                        class="text-light"
                        for="last_name">
                        Apellido 
                        <span 
                            class="text-danger">
                            *
                        </span>
                    </label>
                    <input 
                        class="form-control"
                        id="last_name"
                        type="text"
                        name="last_name"
                        v-model="last_name"
                        placeholder="Ingrese apellido"
                    >
            
                    <label
                        class="text-light" 
                        for="email">
                        Correo 
                        <span 
                            class="text-danger">
                            *
                        </span>
                    </label>
                    <input 
                        class="form-control"
                        id="email"
                        type="email"
                        name="email"
                        v-model="email"
                        @change="verify_email"
                        placeholder="Ingrese correo"
                    >
            
                    <label
                        class="text-light" 
                        for="password">
                        Contraseña 
                        <span 
                            class="text-danger">
                            *
                        </span>
                    </label>
                    <input 
                        class="form-control"
                        id="password"
                        type="password"
                        name="password"
                        v-model="password"
                        placeholder="Ingrese contraseña"
                    >
            
                    <div class="d-grid gap-2 col-6 mx-auto mt-4">
                        <button 
                            class="btn btn-outline-warning"
                            id="register"
                            type="submit"
                            @click.prevent="register">
                            Crear
                        </button>
                        <a 
                            class="btn btn-outline-light"
                            type="button"
                            href="{{ url('/login') }}">
                            Volver
                        </a>             
                    </div>
                </form>
            </div>
    </div>
@endsection
@section('script')
    @include('include.vue')
    <script>
        new Vue({
            el:"#appCreateUser",
            data:{
                name:'',
                last_name:'',
                email:'',
                password:''
            },
            methods: {
                register(){
                    let token = $('input[name=_token]').val()
                    let name = document.getElementById('name')
                    let last_name = document.getElementById('last_name')
                    let email = document.getElementById('email')
                    let password = document.getElementById('password')

                    if(this.validate_input(this.name)){
                        name.style.borderColor = 'red'
                        name.focus()
                        Swal.fire('¡Falta nombre!','Ingrese campo nombre','warning')
                        return
                    }else{
                        name.style.borderColor = ''
                    }

                    if(this.validate_input(this.last_name)){
                        last_name.style.borderColor = 'red'
                        last_name.focus()
                        Swal.fire('¡Falta apellido!','Ingrese campo apellido','warning')
                        return
                    }else{
                        last_name.style.borderColor = ''
                    }

                    if(this.validate_input(this.email)){
                        email.style.borderColor = 'red'
                        email.focus()
                        Swal.fire('¡Falta email!','Ingrese campo correo','warning')
                        return
                    }else{
                        email.style.borderColor = ''
                    }

                    if(this.validate_input(this.password)){
                        password.style.borderColor = 'red'
                        password.focus()
                        Swal.fire('¡Falta contraseña!','Ingrese campo contraseña','warning')
                        return
                    }else{
                        password.style.borderColor = ''
                    }
                
                    axios.post('/users',{
                        _token    : token,
                        name      : this.name,
                        last_name : this.last_name,
                        email     : this.email,
                        password  : this.password
                    }).then(response=>{
                        if(response.data != 0 ){
                            this.cleanForm();
                            Swal.fire('¡Se ha creado cuenta!', 'Correctamente!', 'success'); 
                        }else{
                            Swal.fire('¡No se pudo crear cuenta!', 'Intente de nuevo', 'warning');
                            this.cleanForm();
                            return;
                        }
                    }).catch(error=>{
                        console.log(error);
                    });  
                },
                validate_input(input){
                    return input == '' ? true : false
                },
                verify_email(email){
                    axios.get(`/user/verify-email/${this.email}`)
                    .then(response =>{
                        if(response.data != 0){
                            Swal.fire('¡Correo ingresado!', 'Ya existe', 'warning');
                            this.email = '';
                            document.getElementById('email').focus();
                            return;
                        }
                    }).catch(error=>{
                        console.error(error);
                    });
                },
                cleanForm(){
                    return document.getElementById('form_register').reset();
                },
                pressEnter(){
                    document.addEventListener('keypress',(e)=>{
                        if(e.which == 13){
                            this.register();
                        }
                    })
                }
            },
            mounted() {
                this.pressEnter();
            },

        });
    </script>
@endsection